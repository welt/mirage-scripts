#!/bin/sh
domain=$(cd ~/.scripts && < miragelist dmenu -i -l 20 -fn 'Inconsolata:pixelsize=24' )
maim -s -u -q | mirage -u - -d $domain | xclip -sel clip && notify-send "Successfully uploaded. The image link has been copied to your clipboard." || notify-send "Something went wrong while handling the screenshot."
