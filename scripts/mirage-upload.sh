#!/bin/sh
if [ -z "$1" ]; then
  printf "Please specify a file!\n"
  exit
fi

inputfile=$1
domain="$(<$HOME/.local/share/mirage-upload/domains dmenu -i -l 20 -fn 'Inconsolata:pixelsize=24')"
output="$(mirage -d $domain -u $1)"

case $output in https\:\/\/*)
  printf "%s" "$output" | xclip -sel clipboard
  exit 0
esac

printf "%s" "$output" 

